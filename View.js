import React, { Component } from 'react';
import {
  BackHandler,
  Platform
} from 'react-native';
import {WebView} from 'react-native-webview';

export default class AppView extends Component {
  webView = {
    canGoBack: false,
    ref: null,
  }
  static navigationOptions = {
    header:null
  }  
  onAndroidBackPress = () => {
    if (this.webView.canGoBack && this.webView.ref) {
      this.webView.ref.goBack();
      return true;
    }
    return false;
  }

  componentDidMount() {
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.onAndroidBackPress);
    }
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress');
    }
  }

  render() {
    const { navigation } = this.props;
    return (

      <WebView
        source={{ uri: `https://yuvatharamblooddonors.org${navigation.getParam('action', '')}` }}
        style={{marginTop:22}}
        ref={(webView) => { this.webView.ref = webView; }}
        onNavigationStateChange={(navState) => { this.webView.canGoBack = navState.canGoBack; }}
        />
    );
  }
}