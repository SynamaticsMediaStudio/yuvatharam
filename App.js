import React from 'react';
import { StyleSheet,Image , View, Text, StatusBar, TouchableNativeFeedback } from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import AppView from './View';

class HomeScreen extends React.Component {
  static navigationOptions = {
    header:null
  }
  render() {
    let pic = require('./assets/blood-donation-4165394_1920.jpg');
    let logo = require('./assets/icon.png');
    return (
      <View>
        <StatusBar backgroundColor="#db231b"/>
        <Image source={pic} style={{width: "100%",height:350}}/>
        <View>
          <View style={[styles.buttonGroup]}>

          <TouchableNativeFeedback style={[styles.Button]} onPress={() => {this.props.navigation.navigate('AppView', {action: "#about"});}}>
              <Text style={[styles.ButtonText]} >About</Text>
          </TouchableNativeFeedback>
          <TouchableNativeFeedback style={[styles.Button]} onPress={() => {this.props.navigation.navigate('AppView', {action: "/donor.php"});}}>
              <Text style={[styles.ButtonText]} >Donor Search</Text>
          </TouchableNativeFeedback>
          <TouchableNativeFeedback style={[styles.Button]} onPress={() => {this.props.navigation.navigate('AppView', {action: "/register.php"});}}>
              <Text style={[styles.ButtonText]} >Donor Registration</Text>
          </TouchableNativeFeedback>
          <TouchableNativeFeedback style={[styles.Button]} onPress={() => {this.props.navigation.navigate('AppView', {action: "/login.php"});}}>
              <Text style={[styles.ButtonText]} >Login</Text>
          </TouchableNativeFeedback>
          </View>      
        </View>
        <View style={{ alignItems: 'center', justifyContent: 'center',marginBottom:30 }}>
          <Text style={{fontSize:25,color:"#db231b",fontWeight:"100"}}>Yuvatharam Blood Donors</Text>
          <Image source={logo} style={{width: 80,height:80}}/>
        </View>
      </View>
    );
  }
}
class AuthLoadingScreen extends React.Component {
  componentDidMount(){
      setTimeout(() => {
        this.props.navigation.navigate('App');
      }, 5000);
  }

  render() {
    return (
      <View style={{ flex: 1, alignItems: 'flex-start', justifyContent: 'center', paddingLeft:20 }}>
        <StatusBar hidden={true}/>
        <Text style={{fontSize:25,color:"#db231b",fontWeight:"100"}}>Yuvatharam Blood Donors</Text>
        <Text style={{fontSize:16,color:"#9e9e9e",fontWeight:"100"}}>In The Memory Of Late. Mr Rhudhir Bellala</Text>
      </View>
    );
  }
}


const AppStack = createStackNavigator({ Home: HomeScreen, AppView: AppView });

const AppNavigator = createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading: AuthLoadingScreen,
      App: AppStack,
    },
    {
      initialRouteName: 'AuthLoading',
    }
  )  
);
const styles = StyleSheet.create({
  Button:{
    width:"50%",
  },
  buttonGroup:{

  },
  ButtonText:{
    borderWidth:1,
    borderColor:"#fae3e7",
    color:"#db231b",
    fontSize:15,
    paddingVertical:10,
    textAlign:"center",
    paddingHorizontal:30,
    borderRadius:3,
    marginBottom:10,
    marginHorizontal:1,
  }
})
export default AppNavigator;